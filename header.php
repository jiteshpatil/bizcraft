<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- skip-link -->
<a href="#site-content" class="screen-reader-text"><?php _e( 'Skip to content', 'bizcraft' ); ?></a>
<!-- skip-link -->

<!-- site-header -->
<header id="site-header" role="banner">
	<div class="container">
		<div class="row">
			<!-- brand -->
			<div id="brand" class="col-sm-12 col-bg-4">
				<?php
					if( function_exists( 'jetpack_the_site_logo' ) ):
						jetpack_the_site_logo();
					endif;
					
					if( is_home() && is_front_page() ): ?>
						<h1 class="site-title h2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> <?php
					else: ?>
						<p class="site-title h2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p> <?php
					endif;
				?>
			</div>
			<!-- brand -->
			
			<?php if( has_nav_menu( 'menu-header' ) ): ?>
			
				<!-- header-nav -->
				<nav id="header-nav"  class="col-sm-12 col-bg-8" role="navigation">
					<button id="header-menu-toggle" aria-control="header-menu" aria-expanded="false"><span class="screen-reader-text"><?php _e( 'Header Menu', 'bizcraft' ); ?></span><i class="fa fa-bars"></i></button>
					
					<?php
						wp_nav_menu( array(
							'container' 	 => false,
							'depth' 	 	 => 2,
							'menu_id' 	 	 => 'header-menu',
							'theme_location' => 'menu-header',
						) );
					?>
				</nav>
				<!-- primary-nav -->
				
			<?php endif; ?>
		</div>
	</div>
</header>
<!-- site-header -->