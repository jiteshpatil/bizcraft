<?php get_header(); ?>

<!-- site-content -->
<div id="site-content">
	<div class="container">
		<!-- main -->
		<div id="main" role="main">
			<!-- error-404 -->
			<div class="hentry error-404">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Page not found', 'bizcraft' ); ?></h1>
				</header>
				
				<div class="entry-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'bizcraft' ); ?></p>

					<?php get_search_form(); ?>
				</div>
			</div>
			<!-- error-404 -->
		</div>
		<!-- main -->
	</div>
</div>
<!-- site-content -->

<?php get_footer(); ?>