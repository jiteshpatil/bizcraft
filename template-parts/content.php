<!-- entry -->
<article <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			the_title(
				sprintf( '<h2 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ),
				'</a></h2>'
			);
		?>
		
		<p class="entry-meta">
			<time class="entry-time" datetime="<?php the_time( 'c' ); ?>" rel="bookmark">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo get_the_date(); ?></a>
			</time>
			
			<span class="entry-author">
				<?php
					_e( 'by ', 'bizcraft' );
					the_author_link();
				?>
			</span>
		</p>
	</header>
	
	<?php if( has_post_thumbnail() ): ?>
	
		<div class="entry-image">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
		</div>
	
	<?php endif; ?>
	
	<div class="entry-content">
		<?php 
			the_content( 
				sprintf(  
					__( 'Read More<span class="screen-reader-text"> of %s</span>', 'bizcraft' ),
					get_the_title()
				) 
			); 
		?>
	</div>
</article>
<!-- entry -->