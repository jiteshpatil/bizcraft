<?php
	$bizcraft_featured_posts = bizcraft_get_featured_posts();
	
	if( 1 <= count( $bizcraft_featured_posts ) ): 
		global $post; ?>
	
		<div id="featured-posts" class="cycle-slideshow"
			 data-cycle-slides="> .cycle-slide"
			 data-cycle-fx="scrollHorz"
			 data-cycle-pause-on-hover="true">
		
			<?php foreach( $bizcraft_featured_posts as $post ): setup_postdata( $post ); ?>
				<div class="cycle-slide">
					<div class="featured-post" style="background-image: url( '<?php bizcraft_featured_image_url(); ?>' );">
						<div class="container">
							
							<?php get_template_part( 'template-parts/featured', 'post' ); ?>
							
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			
			<?php if( 1 < count( $bizcraft_featured_posts ) ): ?>
			
				<div class="cycle-prev cycle-button"><i class="fa fa-chevron-left"></i></div>
				<div class="cycle-next cycle-button"><i class="fa fa-chevron-right"></i></div>		
				
			<?php endif; ?>
			
		</div> <?php
		
		wp_reset_postdata();
		
	endif;