<!-- entry -->
<article <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		
		<p class="entry-meta">
			<time class="entry-time" datetime="<?php the_time( 'c' ); ?>" rel="bookmark">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo get_the_date(); ?></a>
			</time>
			
			<span class="entry-author">
				<?php
					_e( 'by ', 'bizcraft' );
					the_author_link();
				?>
			</span>
		</p>
	</header>
	
	<?php if( has_post_thumbnail() ): ?>
	
		<div class="entry-image"><?php the_post_thumbnail(); ?></div>
	
	<?php endif; ?>
	
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	
	<footer class="entry-footer entry-meta">
		<span class="entry-categories">
			<?php
				_e( 'Filed under: ', 'bizcraft' );
				the_category( ', ' );
			?>
		</span>
		
		<?php
			the_tags(  
				'<span class="entry-tags">' . __( 'Tagged with: ', 'bizcraft' ),
				', ', '</span>'
			);
		?>
	</footer>
</article>
<!-- entry -->