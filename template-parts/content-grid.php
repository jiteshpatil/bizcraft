<!-- entry --><article <?php post_class(); ?>>
	<?php if( has_post_thumbnail() ): ?>
	
		<div class="entry-image">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
		</div>
	
	<?php endif; ?>
	
	<?php
		the_title(
			sprintf( 
				'<header class="entry-header"><h2 class="entry-title h4"><a href="%s">', 
				esc_url( get_permalink() ) 
			),
			'</a></h2></header>'
		);
	?>
	
	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div>
</article><!-- entry -->