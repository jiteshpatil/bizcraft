<?php if( is_active_sidebar( 'sidebar-shop' ) ): ?>

	<!-- sidebar-shop -->
	<div id="sidebar" class="sidebar-shop col-sm-12 col-bg-4" role="complementary">
		<?php dynamic_sidebar( 'sidebar-shop' ); ?>
	</div>
	<!-- sidebar-shop -->
	
<?php endif; ?>