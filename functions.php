<?php
/**
 *	Set content width based on theme's design.
 *	----------------------------------------------------------------------------
 */
function bizcraft_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bizcraft_content_width', 690 );
}

add_action( 'after_setup_theme', 'bizcraft_content_width' );

if( ! function_exists( 'bizcraft_setup' ) ):
/**
 *	Add support for theme features.
 *	----------------------------------------------------------------------------
 */
function bizcraft_setup() {
	load_theme_textdomain( 'bizcraft', get_template_directory() . '/languages' );
	
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array(
		'caption',
		'comment-form',
		'comment-list',
		'gallery',
		'search-form',
	) );
	
	add_theme_support( 'jetpack-responsive-videos' );
	add_theme_support( 'site-logo', array(
		'header-text' => 'site-title',
		'size' 		  => 'full',
	) );
	add_theme_support( 'featured-content', array(
		'filter'	  => 'bizcraft_get_featured_posts',
		'max_posts'	  => 3,
		'post_types'  => array( 'page' ),
	) );
	
	register_nav_menus( array(
		'menu-header' => __( 'Header Menu', 'bizcraft' ),
		'menu-footer' => __( 'Footer Menu', 'bizcraft' ),
	) );
}

endif;

add_action( 'after_setup_theme', 'bizcraft_setup' );

if( ! function_exists( 'bizcraft_image_sizes' ) ):
/**
 *	Setup theme image sizes.
 *	----------------------------------------------------------------------------
 */
function bizcraft_image_sizes() {
	set_post_thumbnail_size( 1200, 630, true );
	
	update_option( 'large_size_w', 1200 );
	update_option( 'large_size_h', 9999 );
	
	update_option( 'medium_size_w', 768 );
	update_option( 'medium_size_h', 9999 );
	
	update_option( 'thumbnail_size_w', 480 );
	update_option( 'thumbnail_size_h', 320 );
}

endif;

add_action( 'after_setup_theme', 'bizcraft_image_sizes' );

/**
 *	Register theme sidebars.
 *	----------------------------------------------------------------------------
 */
function bizcraft_sidebars() {
	register_sidebar( array(
		'id'			=> 'sidebar',
		'name'			=> __( 'Sidebar', 'bizcraft' ),
		'before_widget'	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h4 class="widget-title">',
		'after_title'	=> '</h4>',
	) );
	
	register_sidebar( array(
		'id'			=> 'sidebar-footer',
		'name'			=> __( 'Footer', 'bizcraft' ),
		'before_widget'	=> '<aside id="%1$s" class="widget col-sm-12 col-bg-4 %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h4 class="widget-title">',
		'after_title'	=> '</h4>',
	) );
	
	register_sidebar( array(
		'id'			=> 'sidebar-front',
		'name'			=> __( 'Front Page', 'bizcraft' ),
		'before_widget'	=> '<aside id="%1$s" class="widget col-sm-12 col-bg-4 %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h2 class="widget-title h4">',
		'after_title'	=> '</h2>',
	) );
}

add_action( 'widgets_init', 'bizcraft_sidebars' );

/**
 *	Enqueue theme styles & scripts.
 *	----------------------------------------------------------------------------
 */
function bizcraft_scripts() {
	wp_enqueue_style( 'bizcraft-style', get_stylesheet_uri() );
	wp_enqueue_style( 'bizcraft-fonts', 
		apply_filters( 'bizcraft_fonts_url', 
			'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,700,700italic,900|Inconsolata'
		) 
	);
	wp_enqueue_style( 'bizcraft-icons',
		'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'
	);
	
	if( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'bizcraft-match-height', 
		get_template_directory_uri() . '/js/jquery.matchHeight-min.js',
		array( 'jquery' ), null, true
	);
	wp_enqueue_script( 'bizcraft-script', 
		get_template_directory_uri() . '/js/functions.js',
		array( 'bizcraft-match-height' ), null, true
	);
	
	if( bizcraft_has_featured_posts( 2 ) ) {
		wp_enqueue_script( 'bizcraft-cycle2', 
			get_template_directory_uri() . '/js/jquery.cycle2.min.js',
			array( 'jquery' ), null, true
		);
	}
}

add_action( 'wp_enqueue_scripts', 'bizcraft_scripts' );

if( ! function_exists( 'bizcraft_excerpt_length' ) ):
/**
 *	Custom excerpt length.
 *	----------------------------------------------------------------------------
 */
function bizcraft_excerpt_length( $length ) {
	return 30;
}

endif;

add_filter( 'excerpt_length', 'bizcraft_excerpt_length' );

if( ! function_exists( 'bizcraft_excerpt_more' ) ):
/**
 *	Custom excerpt more link.
 *	----------------------------------------------------------------------------
 */
function bizcraft_excerpt_more( $more ) {
	return sprintf( 
		__( '&hellip;<p><a href="%1$s" class="more-link">Read More<span class="screen-reader-text"> of %2$s</span></a></p>', 'bizcraft' ),
		esc_url( get_permalink() ), get_the_title()
	);
}

endif;

add_filter( 'excerpt_more', 'bizcraft_excerpt_more' );

/**
 *	Custom body class.
 *	----------------------------------------------------------------------------
 */
function bizcraft_body_class( $classes ) {
	if( ! is_active_sidebar( 'sidebar' ) ) {
		$classes[] = 'no-sidebar';
	}
	
	return $classes;
}

add_filter( 'body_class', 'bizcraft_body_class' );

/**
 *	Echo featured image url.
 *	----------------------------------------------------------------------------
 */
function bizcraft_featured_image_url( $post_id = 0, $size = 'full' ) {
	if( ! $post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	
	if( has_post_thumbnail( $post_id ) ) {
		$thumbnail_id = get_post_thumbnail_id( $post_id );
		$featured_image = wp_get_attachment_image_src( $thumbnail_id, $size );
		
		echo esc_url( $featured_image[0] );
	}
}

/**
 *	Customizer options.
 *	----------------------------------------------------------------------------
 */
function bizcraft_customizer_options( $wp_customize ) {
	// Accent Color
	$wp_customize->add_setting( 'bizcraft_accent_color', array(
		'default'			=> '#1F9EA3',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_accent_color', array(
			'label'			=> __( 'Accent Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
	
	// Alternate Accent Color
	$wp_customize->add_setting( 'bizcraft_alt_accent_color', array(
		'default'			=> '#3B0102',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_alt_accent_color', array(
			'label'			=> __( 'Alternate Accent Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
	
	// Highlight Color
	$wp_customize->add_setting( 'bizcraft_highlight_color', array(
		'default'			=> '#EBE9DD',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_highlight_color', array(
			'label'			=> __( 'Highlight Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
	
	// Header background Color
	$wp_customize->add_setting( 'bizcraft_header_background_color', array(
		'default'			=> '#3B0102',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_header_background_color', array(
			'label'			=> __( 'Header Background Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
	
	// Header text Color
	$wp_customize->add_setting( 'bizcraft_header_text_color', array(
		'default'			=> '#FFFFFF',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_header_text_color', array(
			'label'			=> __( 'Header Text Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
	
	// Featured Content Overlay Color
	$wp_customize->add_setting( 'bizcraft_feature_overlay_color', array(
		'default'			=> '#000000',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_feature_overlay_color', array(
			'label'			=> __( 'Featured Content Overlay Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
	
	// Featured Content Overlay Opacity
	$wp_customize->add_setting( 'bizcraft_feature_overlay_opacity', array(
		'default'			=> '0.8',
	) );
	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'bizcraft_feature_overlay_opacity', array(
			'label'			=> __( 'Featured Content Overlay Color', 'bizcraft' ),
			'section'		=> 'colors',
			'type'			=> 'select',
			'choices'	=> array(
				'0'		=> __( 'Transparent', 'bizcraft' ),
				'0.2'	=> __( '20%', 'bizcraft' ),
				'0.4'	=> __( '40%', 'bizcraft' ),
				'0.6'	=> __( '60%', 'bizcraft' ),
				'0.8'	=> __( '80%', 'bizcraft' ),
				'1'		=> __( '100%', 'bizcraft' ),
			),
		)
	) );
	
	// Featured Content Text Color
	$wp_customize->add_setting( 'bizcraft_feature_text_color', array(
		'default'			=> '#FFFFFF',
		'sanitize_callback'	=> 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control(
		$wp_customize, 'bizcraft_feature_text_color', array(
			'label'			=> __( 'Featured Content Text Color', 'bizcraft' ),
			'section'		=> 'colors',
		)
	) );
}

add_action( 'customize_register', 'bizcraft_customizer_options' );

/**
 *	Customizer output.
 *	----------------------------------------------------------------------------
 */
function bizcraft_customizer_output() {
	$accent_color = get_theme_mod( 'bizcraft_accent_color', '#1F9EA3' );
	$alt_accent_color = get_theme_mod( 'bizcraft_alt_accent_color', '#3B0102' );
	$highlight_color = get_theme_mod( 'bizcraft_highlight_color', '#EBE9DD' );
	$feature_overlay_color = get_theme_mod( 'bizcraft_feature_overlay_color', '#000000' );
	$feature_overlay_opacity = get_theme_mod( 'bizcraft_feature_overlay_opacity', '0.8' );
	$feature_text_color = get_theme_mod( 'bizcraft_feature_text_color', '#FFFFFF' );
	$header_bg_color = get_theme_mod( 'bizcraft_header_background_color', '#3B0102' );
	$header_text_color = get_theme_mod( 'bizcraft_header_text_color', '#FFFFFF' );
	
	ob_start(); ?>

<style id="bizcraft-styles" type="text/css">
	a { 
		color: <?php echo $accent_color ?>; 
	}

	a:hover { 
		border-bottom-color: <?php echo $accent_color ?>; 
	}

	.button, 
	.button-big, 
	.button-alt, 
	.button-alt-big,
	#header-menu-toggle,
	input[type=submit] { 
		background-color: <?php echo $accent_color ?>; 
	}

	.button:hover, 
	.button-big:hover,
	input[type=submit]:hover { 
		background-color: <?php echo $alt_accent_color ?>; 
	}
	
	@media( min-width: 990px ) {
		#header-menu .sub-menu {
			background-color: <?php echo $header_bg_color ?>; 
		}
	}

	.button-alt:hover, 
	.button-alt-big:hover { 
		background-color: <?php echo $accent_color ?>; 
	}

	#site-header { 
		background-color: <?php echo $header_bg_color ?>; 
	}

	#site-header a { 
		color: <?php echo $header_text_color ?>; 
	}

	#site-header a:hover,
	#header-menu .current-menu-item > a,
	.entry-header a:hover { 
		color: <?php echo $accent_color ?>; 
	}

	.entry-header a,
	.entry-title { 
		color: <?php echo $alt_accent_color ?>; 
	}

	#sidebar-footer { 
		background-color: <?php echo $alt_accent_color ?>; 
	}

	.wpcf7, 
	.comment-repond, 
	#page-header, 
	#sidebar-front, 
	address, 
	blockquote, 
	code, 
	figure, 
	kbd, 
	pre, 
	tt, 
	var { 
		background-color: <?php echo $highlight_color ?>; 
	}

	#featured-posts .featured-post:before { 
		background-color: <?php echo $feature_overlay_color ?>; 
		opacity: <?php echo $feature_overlay_opacity ?>; 
	}
	
	#featured-posts .cycle-button, 
	#featured-posts .featured-post, 
	#featured-posts .featured-post .entry-title, 
	#featured-posts .featured-post h1, 
	#featured-posts .featured-post .h1, 
	#featured-posts .featured-post h2, 
	#featured-posts .featured-post .h2, 
	#featured-posts .featured-post h3, 
	#featured-posts .featured-post .h3, 
	#featured-posts .featured-post h4, 
	#featured-posts .featured-post .h4, 
	#featured-posts .featured-post h5, 
	#featured-posts .featured-post .h5, 
	#featured-posts .featured-post h6, 
	#featured-posts .featured-post .h6 {
		color: <?php echo $feature_text_color ?>;
	}
</style> <?php

	echo ob_get_clean() . "\n\n";
}

add_action( 'wp_head', 'bizcraft_customizer_output' );

/**
 *	Return an array() of featured posts.
 *	----------------------------------------------------------------------------
 */
function bizcraft_get_featured_posts() {
	return apply_filters( 'bizcraft_get_featured_posts', array() );
}

/**
 *	Check if the site has featured posts.
 *	----------------------------------------------------------------------------
 */
function bizcraft_has_featured_posts( $minimum = 1 ) {
	if ( is_paged() ) {
        return false;
	}	
 
    $minimum = absint( $minimum );
    $featured_posts = apply_filters( 'bizcraft_get_featured_posts', array() );
 
    if ( ! is_array( $featured_posts ) ) {
        return false;
	}
 
    if ( $minimum > count( $featured_posts ) ) {
        return false;
	}
 
    return true;
}

/**
 *	Check if a particular plugin is active.
 *	----------------------------------------------------------------------------
 */
function bizcraft_is_plugin_active( $plugin_name ) {
	$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
	
	if( in_array( $plugin_name, $active_plugins ) ) {
		return true;
	}
	
	return false;
}