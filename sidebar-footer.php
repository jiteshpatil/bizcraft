<?php if( is_active_sidebar( 'sidebar-footer' ) ): ?>

	<!-- sidebar-footer -->
	<div id="sidebar-footer" role="complementary">
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar( 'sidebar-footer' ); ?>
			</div>
		</div>
	</div>
	<!-- sidebar-footer -->
	
<?php endif; ?>