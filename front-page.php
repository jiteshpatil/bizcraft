<?php 
	get_header(); 
	
	if( bizcraft_is_plugin_active( 'jetpack/jetpack.php' ) ):
		get_template_part( 'template-parts/featured', 'content' );
	endif;
	
	if( is_active_sidebar( 'sidebar-front' ) ):
		get_sidebar( 'front' );
	endif;
	
	if( 'posts' == get_option( 'show_on_front' ) ):
		include( get_home_template() );
	else:
		include( get_page_template() );
	endif;
	
	get_footer();
?>