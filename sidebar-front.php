<?php if( is_active_sidebar( 'sidebar-front' ) ): ?>

	<!-- sidebar-front -->
	<div id="sidebar-front" role="complementary">
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar( 'sidebar-front' ); ?>
			</div>
		</div>
	</div>
	<!-- sidebar-front -->
	
<?php endif; ?>