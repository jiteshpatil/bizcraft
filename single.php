<?php get_header(); ?>

<!-- site-content -->
<div id="site-content">
	<div class="container">
		<div class="row">
			<!-- main -->
			<div id="main" class="col-sm-12 col-bg-8" role="main">
				<?php while( have_posts() ): the_post(); ?>
						
					<?php get_template_part( 'template-parts/content', 'single' ); ?>
					
					<?php the_post_navigation(); ?>
					
					<?php 
						if( comments_open() || get_comments_number() ):	
							comments_template(); 
						endif;
					?>
						
				<?php endwhile; ?>
			</div>
			<!-- main -->
			
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<!-- site-content -->

<?php get_footer(); ?>