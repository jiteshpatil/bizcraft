<?php if( is_active_sidebar( 'sidebar' ) ): ?>

	<!-- sidebar -->
	<div id="sidebar" class="col-sm-12 col-bg-4" role="complementary">
		<?php dynamic_sidebar( 'sidebar' ); ?>
	</div>
	<!-- sidebar -->
	
<?php endif; ?>