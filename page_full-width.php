<?php 
	/**
	 *	Template name: Full Width Template
	 *	------------------------------------------------------------------------
	 */
	get_header(); 
?>

<!-- site-content -->
<div id="site-content">
	<div class="container">
		<!-- main -->
		<div id="main" role="main">
			<?php while( have_posts() ): the_post(); ?>
					
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				
				<?php 
					if( comments_open() || get_comments_number() ):	
						comments_template(); 
					endif;
				?>
					
			<?php endwhile; ?>
		</div>
		<!-- main -->
	</div>
</div>
<!-- site-content -->

<?php get_footer(); ?>