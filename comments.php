<?php
	if( post_password_required() ):
		return;
	endif;
?>

<div id="comments">
	<?php
		if( have_comments() ): ?>
			<h2 id="comments-title">
				<?php comments_number( '', __( 'Comments', 'bizcraft' ), __( 'Comments (%)', 'bizcraft' ) ); ?>
			</h2>
			
			<ol id="comments-list">
				<?php
					wp_list_comments( array(
						'avatar_size' => 50,
						'short_ping'  => true,
						'style'   	  => 'ol',
					) );
				?>
			</ol> <?php
		endif;
		
		if( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ): ?>
			<p class="comments-closed highlight">
				<?php _e( 'Comments are closed.', 'bizcraft' ); ?>
			</p> <?php
		endif;
		
		comment_form();
	?>
</div>