<?php get_header(); ?>

<!-- site-content -->
<div id="site-content">
	<?php if( is_home() && ! is_front_page() ): ?>
	
		<!-- page-header -->
		<header id="page-header" class="screen-reader-text">
			<div class="container">
				<h1 id="page-title" class="h6"><?php single_post_title(); ?></h1>
			</div>
		</header>
		<!-- page-header -->
		
	<?php endif; ?>
	
	<div class="container">
		<div class="row">
			<!-- main -->
			<div id="main" class="col-sm-12 col-bg-8" role="main">
				<?php if( have_posts() ): ?>
					
					<?php while( have_posts() ): the_post(); ?>
							
						<?php get_template_part( 'template-parts/content' ); ?>
							
					<?php endwhile; ?>
					
					<?php the_posts_navigation(); ?>
					
				<?php else: ?>
				
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				
				<?php endif; ?>
			</div>
			<!-- main -->
			
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<!-- site-content -->

<?php get_footer(); ?>