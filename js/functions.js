( function( $ ) {
	
	// Toggle header menu
	$( '#header-menu-toggle' ).click( function( e ) {
		if( 'false' == $( this ).attr( 'aria-expanded' ) ) {
			$( this ).attr( 'aria-expanded', 'true' );
			$( '#header-menu' ).slideDown();
		}
		else {
			$( this ).attr( 'aria-expanded', 'false' );
			$( '#header-menu' ).slideUp();
		}
		e.preventDefault();
	} );
	
	// Sub menu toggle
	$( '#header-menu li.menu-item-has-children' ).append( '<span class="sub-menu-expander"><i class="fa fa-plus-circle"></i></span>' );
	
	$( '#header-menu li.menu-item-has-children > span.sub-menu-expander' ).bind( 'click', function( e ) {
		$( this ).prev( '.sub-menu' ).slideToggle();
		$( this ).find( '.fa' ).toggleClass( 'fa-plus-circle' ).toggleClass( 'fa-minus-circle' );
		e.preventDefault();
	} );
	
	// Match height
	$( '.match-height' ).matchHeight();
} )( jQuery );