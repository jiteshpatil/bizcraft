<?php get_sidebar( 'footer' ); ?>

<!-- site-footer -->
<footer id="site-footer">
	<div class="container">
		<?php if( has_nav_menu( 'menu-footer' ) ): ?>
		
			<!-- footer-nav -->
			<nav id="footer-nav" role="navigation">
				<?php
					wp_nav_menu( array(
						'container' 	 => false,
						'depth' 	 	 => 1,
						'menu_id' 	 	 => 'footer-menu',
						'theme_location' => 'menu-footer',
					) );
				?>
			</nav>
			<!-- footer-nav -->
			
		<?php endif; ?>
		
		<!-- copyright -->
		<div id="copyright">
			<?php
				echo wp_kses_post(
					apply_filters( 'bizcraft_copyright',
						sprintf( __( 'Copyright &copy; %s', 'bizcraft' ),
							get_bloginfo( 'name' )
						)
					)
				);
			?>
		</div>
		<!-- copyright -->
	</div>
</footer>
<!-- site-footer -->

<?php wp_footer(); ?>
</body>
</html>